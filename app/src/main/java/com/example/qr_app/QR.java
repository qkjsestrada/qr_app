package com.example.qr_app;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.example.qr_app.Utils.JsonPlaceHolder;
import com.example.qr_app.Utils.Post;
import com.example.qr_app.Utils.StudentData;
import com.google.zxing.Result;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class QR extends AppCompatActivity {
    private CodeScanner mCodeScanner;
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    private Button scanagainBtn;
    private TextView datetimeTV, decodedText;
    private JsonPlaceHolder jsonPlaceHolder;
    private String SavedUniqueLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);

        Paper.init(this);

        initWidgets();
        setupPermission();

        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
        String datetime = dateformat.format(c.getTime());
        datetimeTV.setText(datetime);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolder = retrofit.create(JsonPlaceHolder.class);

//        getPosts();

        Retrofit retrofit1 = new Retrofit.Builder()
                .baseUrl("https://webqc.tip.edu.ph/portal/student/new/qr/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolder = retrofit1.create(JsonPlaceHolder.class);

        jsonPlaceHolder.getTIP().enqueue(new Callback<StudentData>() {
            @Override
            public void onResponse(Call<StudentData> call, Response<StudentData> response) {
                if (!response.isSuccessful()) {
                    decodedText.setText("Code: " + response.code());
                    return;
                }

                String content = "";
                content += "DATE: " + response.body().getDate() + "\n";
                content += "NAME: " + response.body().getStudentName() + "\n";
                content += "CTR: " + response.body().getCtr() + "\n\n";

                decodedText.append(content);

            }

            @Override
            public void onFailure(Call<StudentData> call, Throwable t) {
                decodedText.setText(t.getMessage());
            }
        });

        CodeScannerView scannerView = findViewById(R.id.scanner_view);
        mCodeScanner = new CodeScanner(this, scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        scanagainBtn.setVisibility(View.VISIBLE);
//                        decodedText.setText(result.getText());
                        SavedUniqueLink = result.getText();
                        Paper.book().write("uniquelink", SavedUniqueLink);

                    }
                });
            }
        });

        scanagainBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });

        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });
    }

    private void getPosts(){
        Call<List<Post>> call = jsonPlaceHolder.getPosts();
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {

                if (!response.isSuccessful()) {
                    decodedText.setText("Code: " + response.code());
                    return;
                }

                List<Post> posts = response.body();

                for (Post post : posts) {
                    String content = "";
                    content += "ID: " + post.getId() + "\n";
                    content += "User ID: " + post.getUserId() + "\n";
                    content += "Title: " + post.getTitle() + "\n";
                    content += "Text: " + post.getText() + "\n\n";

                    decodedText.append(content);
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                decodedText.setText(t.getMessage());
            }
        });
    }

    private void initWidgets() {
        scanagainBtn = findViewById(R.id.scanagain_Btn);
        decodedText = findViewById(R.id.decodedTextQR);
        datetimeTV = findViewById(R.id.qrfirstTV);
    }

    private void setupPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Camera Permission Granted", Toast.LENGTH_LONG).show();
            } else {
                Intent qrhomeactivity = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(qrhomeactivity);
                setupPermission();
                Toast.makeText(this, "Camera Permission Denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCodeScanner.startPreview();
    }

    @Override
    protected void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }


}