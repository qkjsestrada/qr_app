package com.example.qr_app.Utils;

import java.util.List;

import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonPlaceHolder {

    String uniquelink = Paper.book().read("uniquelink");

    @GET("posts")
    Call<List<Post>> getPosts();

    @GET("?p=https://qcis.tip.edu.ph/survey/health_declaration.php?p=774a57463862592f75764a466e4b4c6c576366314a30475a455770344c56326468704653697657492b365136525a4b533571483437672f4d71634f64686d4f466d703941486759366e4c7a455877424c58725a434a5438666a68704a6a636845483437387331446a702b633d&loc=QCAURORAGATE")
    Call<StudentData> getTIP();

}
