package com.example.qr_app.Utils;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StudentData {

    private String date;
    private int ctr;

    @SerializedName("name")
    private String studentName;

    public String getDate() {
        return date;
    }

    public int getCtr() {
        return ctr;
    }

    public String getStudentName() {
        return studentName;
    }
}
